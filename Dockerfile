FROM python:3.5.2

RUN apt-get update && apt-get install -y netcat

RUN mkdir /videosync
COPY requirements.txt /videosync/
WORKDIR /videosync
RUN pip3 install -r requirements.txt
COPY . /videosync/

CMD ["bash", "-c", "bash wait-for-db.sh && gunicorn --worker-class eventlet --bind 0.0.0.0:80 -w 1 app:app"]
