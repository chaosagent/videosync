import os

from flask import Flask

import flask_config
import views
from filters import filters
from models import db, login_manager
from sockets import socketio

app = Flask(__name__, static_url_path='')
self_path = os.path.dirname(os.path.abspath(__file__))
app.config.from_object(flask_config.FlaskConfig(app_root=self_path))

db.init_app(app)
login_manager.init_app(app)
socketio.init_app(app, message_queue=app.config['REDIS_URI'])

app.register_blueprint(views.base.blueprint)
app.register_blueprint(views.player.blueprint, url_prefix='/player')
app.register_blueprint(views.users.blueprint)

app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True

for name, func in filters.items():
    app.add_template_filter(func, name=name)
