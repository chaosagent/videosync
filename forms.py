from flask_wtf import Form
from flask_login import current_user
from sqlalchemy import func
from wtforms import ValidationError
from wtforms.fields import *
from wtforms.validators import *

import constants
import util
from models import User


class LoginForm(Form):
    identifier = StringField('Username', validators=[InputRequired()])
    password = PasswordField('Password', validators=[InputRequired()])

    def get_user(self, identifier=None):
        return User.get_by_identifier(identifier or self.identifier.data)

    def validate_identifier(self, field):
        if self.get_user(field.data) is None:
            raise ValidationError('Invalid identifier')

    def validate_password(self, field):
        user = self.get_user(self.identifier.data)
        if not user:
            return
        if not user.check_password(field.data):
            raise ValidationError('Invalid password')


# TODO: validate email and username uniqueness
class RegisterForm(Form):
    email = StringField('Email', validators=[InputRequired()])
    username = StringField('Username', validators=[InputRequired(), Length(min=4, max=16,
                                                                           message='Username must be between 4 and 16 characters long.')])
    password = PasswordField('Password', validators=[InputRequired(), Length(min=8, max=56,
                                                                             message='Password must be between 8 and 56 characters long.')])


def validate_email(self, field):
    if not util.validate_email_format(field.data):
        raise ValidationError('Invalid email')
    if User.query.filter(func.lower(User.email) == func.lower(field.data)).count():
        raise ValidationError('Email taken!')


def validate_username(self, field):
    if not util.validate_username_format(field.data):
        raise ValidationError('Invalid username')
    if User.query.filter(func.lower(User.username) == func.lower(field.data)).count():
        raise ValidationError('Username taken!')


class PasswordForgotForm(Form):
    email = StringField('Email', validators=[InputRequired()])

    def __init__(self):
        super(PasswordForgotForm, self).__init__()
        self._user = None
        self._user_cached = False

    @property
    def user(self):
        if not self._user_cached:
            self._user = User.query.filter(func.lower(User.email) == self.email.data.lower()).first()
            self._user_cached = True
        return self._user

    def validate_email(self, field):
        if not util.validate_email_format(field.data):
            raise ValidationError('Invalid email')


class PasswordResetForm(Form):
    password = PasswordField('New Password', validators=[InputRequired(), Length(min=8, max=56,
                                                                                 message='Password must be between 8 and 56 characters long.')])
    password_confirm = PasswordField('Confirm Password',
                                     validators=[InputRequired(), EqualTo('password', message='Passwords must match.')])


class NewPlayerSessionForm(Form):
    video_url = StringField('Video URL', validators=[URL(), Length(max=256, message='URL too long!')])
    moderated = BooleanField('Moderated', default=False)

    def validate_video_url(self, field):
        if not any(field.data.startswith(prefix) for prefix in constants.VIDEO_URL_PREFIXES):
            raise ValidationError('Video URL must start with "http://" or "https://"!')

    def validate_moderated(self, field):
        if field.data is True and not current_user.is_authenticated:
            raise ValidationError('Please log in to create a moderated player.')
