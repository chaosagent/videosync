"""Add player moderation

Revision ID: 687cbfab74d7
Revises: ffc68adca07e
Create Date: 2017-11-17 22:08:26.506140

"""

# revision identifiers, used by Alembic.
revision = '687cbfab74d7'
down_revision = 'ffc68adca07e'

import sqlalchemy as sa
from alembic import op


def upgrade():
    op.add_column('player_sessions', sa.Column('moderated', sa.Boolean(), nullable=False))
    op.add_column('player_sessions', sa.Column('owner_id', sa.Integer(), nullable=True))
    op.create_foreign_key('player_session_owner_fk', 'player_sessions', 'users', ['owner_id'], ['id'])


def downgrade():
    op.drop_constraint('player_session_owner_fk', 'player_sessions', type_='foreignkey')
    op.drop_column('player_sessions', 'owner_id')
    op.drop_column('player_sessions', 'moderated')
