import time
from datetime import datetime

from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects import mysql
from sqlalchemy.ext.hybrid import hybrid_property

import config
import util

db = SQLAlchemy()
login_manager = LoginManager()


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Unicode(length=128), unique=True)
    email = db.Column(db.Unicode(length=128), unique=True)
    _password = db.Column('password', db.String(length=60))  # password hash
    admin = db.Column(db.Boolean, default=False)
    _joined = db.Column('joined', db.DateTime, default=datetime.utcnow)

    def __eq__(self, other):
        if isinstance(other, User):
            return self.id == other.id
        return NotImplemented

    '''Python 3 implicitly sets __hash__ to None if __eq__ is overridden. Set back to default implementation.'''

    def __hash__(self):
        return object.__hash__(self)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return '<User %r>' % self.username

    @hybrid_property
    def joined(self):
        return int(time.mktime(self._joined.timetuple()))

    @property
    def is_active(self):
        return True

    @property
    def is_authenticated(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    @classmethod
    def get_by_id(cls, id):
        query_results = cls.query.filter_by(id=id)
        return query_results.first() if query_results.count() else None

    @staticmethod
    @login_manager.user_loader
    def get_user_by_id(id):
        return User.get_by_id(id)

    @classmethod
    def get_by_identifier(cls, identifier):
        if '@' in identifier:  # identifier is email
            query_results = cls.query.filter_by(email=identifier)
        else:  # identifier is username
            query_results = cls.query.filter_by(username=identifier)
        return query_results.first() if query_results.count() else None

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = util.hash_password(password)

    def check_password(self, password):
        return util.verify_password(password, self.password)


class PasswordResetToken(db.Model):
    __tablename__ = 'password_reset_tokens'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', name='pwd_reset_token_user_id_fk'))
    user = db.relationship('User', backref=db.backref('password_reset_tokens', lazy='dynamic'), lazy='joined')
    active = db.Column(db.Boolean)
    token = db.Column(db.String(length=16), default=util.partial(util.generate_string, 16))
    email = db.Column(db.Unicode(length=128))
    expire = db.Column(db.DateTime)

    @property
    def expired(self):
        return datetime.now() >= self.expire


class PlayerSession(db.Model):
    __tablename__ = 'player_sessions'
    id = db.Column(db.Integer, primary_key=True)
    creation_time = db.Column(db.DateTime, default=datetime.utcnow)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id', name='player_session_owner_fk'), nullable=True)
    owner = db.relationship(User, lazy='joined')
    token = db.Column(db.String(length=16), default=util.partial(util.generate_string, config.TOKEN_LENGTH),
                      unique=True, index=True)
    video_url = db.Column(db.UnicodeText())
    is_playing = db.Column(db.Boolean, default=False)
    # if is_playing, unix time of the supposed start of video with no interruptions
    # else, current seek
    # TODO: make pause handling not disgusting
    time_base = db.Column(mysql.DATETIME(fsp=3))

    moderated = db.Column(db.Boolean, default=False, nullable=False)

    @classmethod
    def create(cls, owner_id=None, video_url=None, moderated=False, commit=True):
        player_session = cls(
            owner_id=owner_id,
            video_url=video_url,
            moderated=moderated
        )
        db.session.add(player_session)
        if commit:
            db.session.commit()
        return player_session

    @classmethod
    def get_by_token(cls, token):
        return cls.query.filter_by(token=token).first()

    def can_interact(self, user):
        # FIXME: why doesnt self.owner != user work?
        return not (self.moderated and ((not user.is_authenticated) or self.owner_id != user.id))
