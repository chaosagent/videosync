from datetime import datetime

from flask import request
from flask_login import current_user
from flask_socketio import emit, join_room, SocketIO

from models import db, PlayerSession

socketio = SocketIO()


def get_room_name(token):
    return 'player_{}'.format(token)


def emit_state_update(player_session, individual=False):
    data_payload = {
        'is_playing': player_session.is_playing,
        'time_base': player_session.time_base.timestamp() if player_session.time_base else None,
    }
    if individual:
        data_payload['can_interact'] = player_session.can_interact(current_user)
    else:
        data_payload['origin'] = request.sid
    room = None if individual else get_room_name(player_session.token)
    emit('state_update', data_payload, room=room)


@socketio.on('join_player')
def handle_join_player(token: str):
    player_session = PlayerSession.get_by_token(token)
    if not player_session:
        emit('error', 'Session does not exist!')
        return
    join_room(get_room_name(token))
    emit_state_update(player_session, individual=True)


@socketio.on('get_state')
def handle_get_state(token: str):
    player_session = PlayerSession.get_by_token(token)
    if not player_session:
        emit('error', 'Session does not exist!')
        return
    emit_state_update(player_session, individual=True)


@socketio.on('pause')
def handle_pause(token: str, current_seek: float):
    player_session = PlayerSession.get_by_token(token)
    if not player_session:
        emit('error', 'Session does not exist!')
        return
    if not player_session.can_interact(current_user):
        emit('error', 'You do not have permissions to interact with the player!')
        return
    player_session.is_playing = False
    player_session.time_base = datetime.fromtimestamp(current_seek)
    db.session.commit()

    emit_state_update(player_session)


@socketio.on('play')
def handle_play(token: str, time_base: float):
    player_session = PlayerSession.get_by_token(token)
    if not player_session:
        emit('error', 'Session does not exist!')
        return
    if not player_session.can_interact(current_user):
        emit('error', 'You do not have permissions to interact with the player!')
        return
    player_session.is_playing = True
    player_session.time_base = datetime.fromtimestamp(time_base)
    db.session.commit()

    emit_state_update(player_session)


@socketio.on('seek')
def seek(token: str, time_base: float):
    player_session = PlayerSession.get_by_token(token)
    if not player_session:
        emit('error', 'Session does not exist!')
        return
    if not player_session.can_interact(current_user):
        emit('error', 'You do not have permissions to interact with the player!')
        return
    player_session.time_base = datetime.fromtimestamp(time_base)
    db.session.commit()

    emit_state_update(player_session)
