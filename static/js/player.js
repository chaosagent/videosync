function unix_millis() {
    return (new Date()).getTime() / 1000;
}

function get_time_base(timestamp) {
    return unix_millis() - timestamp;
}

function PlayerController(token, video_player) {
    this.token = token;
    this.video_player = video_player;
    this.socket = null;
    this.current_time = 0;
    this.expected_events = {
        play: 0,
        pause: 0,
        seek: 0
    };
    this.can_interact = false;
}

PlayerController.prototype.start = function () {
    var socket = io(window.location.host);
    this.socket = socket;

    self = this;

    socket.on('state_update', function (data) {
        if (data['origin'] == socket.io.engine.id)
            return;

        if (data['can_interact'] !== null) {
            self.can_interact = data['can_interact'];
            console.log(data['can_interact']);
        }

        if (!data['is_playing'] && !self.video_player.paused()) {
            self.video_player.pause();
            self.expected_events.pause++;
        }

        if (data['is_playing'] === true) {
            self.video_player.currentTime(unix_millis() - data['time_base']);
        } else if (data['is_playing'] === false) {
            self.video_player.currentTime(data['time_base']);
        }
        self.expected_events['seek']++;

        if (data['is_playing'] && self.video_player.paused()) {
            self.video_player.play();
            self.expected_events.play++;
        }
    });


    this.video_player.on('play', function () {
        if (self.expected_events.play) {
            self.expected_events.play--;
            return;
        }
        if (!self.can_interact) {
            return;
        }
        socket.emit('play', self.token, get_time_base(this.currentTime()));
    });

    this.video_player.on('pause', function () {
        if (self.expected_events.pause) {
            self.expected_events.pause--;
            return;
        }
        if (!self.can_interact) {
            return;
        }
        socket.emit('pause', self.token, this.currentTime());
    });

    this.video_player.on('seeked', function () {
        if (self.expected_events.seek) {
            self.expected_events.seek--;
            return;
        }
        if (!self.can_interact) {
            return;
        }
        socket.emit('seek', self.token, get_time_base(this.currentTime()));
    });

    socket.emit('join_player', this.token);
};