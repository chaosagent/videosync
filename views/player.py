from flask import abort, Blueprint, redirect, render_template, url_for, flash
from flask_login import current_user

from forms import NewPlayerSessionForm
from models import PlayerSession

blueprint = Blueprint('player', __name__, template_folder='templates')


@blueprint.route('/new', methods=['GET', 'POST'])
def new():
    new_player_session_form = NewPlayerSessionForm()
    if not current_user.is_authenticated:
        new_player_session_form.moderated.render_kw = {'disabled': ''}
    if new_player_session_form.validate_on_submit():
        player_session = PlayerSession.create(
            owner_id=current_user.id if current_user.is_authenticated else None,
            video_url=new_player_session_form.video_url.data,
            moderated=new_player_session_form.moderated.data,
        )
        return redirect(url_for('.player', token=player_session.token))
    return render_template('player/new.html', new_player_session_form=new_player_session_form)


@blueprint.route('/<string:token>')
def player(token: str):
    player_session = PlayerSession.get_by_token(token)
    if not player_session:
        abort(404)
    return render_template('player/player.html', player_session=player_session)
